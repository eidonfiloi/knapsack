import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@RunWith(JUnit4.class)
public class TestAll {

    private static Logger logger = Logger.getLogger(TestAll.class);

    private static BufferedReader reader;

    private static BufferedReader readerTwo;

    @Test
    public void testAll() throws IOException {

        boolean foundBadPairs = false;
        try {
            reader = new BufferedReader(new FileReader("src/main/resources/packages.txt"));
            Pattern p = Pattern.compile("\\((.*?)\\)",Pattern.DOTALL);
            String nextLine;
            int index = 0;
            packagesWhile:
            while((nextLine = reader.readLine()) != null) {
                String[] lineArr = nextLine.split(":");
                Integer currentMaxWeight = Integer.parseInt(lineArr[0]);
                if(currentMaxWeight <= 0 || currentMaxWeight > 100) {
                    logger.error("The max weight value is invalid. It must be positive and less or equal to 100...");
                    continue packagesWhile;
                }
                Matcher matcher = p.matcher(lineArr[1]);
                KnapsackInstance currentInstance = new KnapsackInstance(currentMaxWeight);

                while(matcher.find()) {

                    String[] thingStrArr = matcher.group(1).split(",");
                    Thing thing = new Thing(Integer.parseInt(thingStrArr[0]), Double.parseDouble(thingStrArr[1]), Double.parseDouble(thingStrArr[2]));
                    currentInstance.addThing(thing);
                }

                currentInstance.initiateInstance();
                ArrayList<String> currentSolution = currentInstance.getSolution();
                logger.debug(currentSolution);
                if(index == 0) {
                    org.junit.Assert.assertTrue("it should be 4...", currentSolution.get(0).equals("4"));
                } else if(index == 1) {
                    org.junit.Assert.assertTrue("it should be empty...", currentSolution.size() == 0);
                } else if(index == 2) {
                    org.junit.Assert.assertTrue("it should be empty...", currentSolution.get(0).equals("2") && currentSolution.get(1).equals("7"));
                } else if(index == 3) {
                    org.junit.Assert.assertTrue("it should be empty...", currentSolution.get(0).equals("9") && currentSolution.get(1).equals("8"));
                }
                index++;
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if(reader != null) {
                reader.close();
            }

        }
    }
}

/**
 * Created with IntelliJ IDEA.
 * User: ptoth
 * Date: 6/24/14
 * Time: 2:57 PM
 */
public class Thing implements Comparable<Thing> {

    private int index;

    private double weight;

    private double price;

    public Thing(int index, double weight, double price) {
        this.index = index;
        if(weight < 0.0 || weight > 100.0 || price < 0.0 || price > 100.0) {
            throw new IllegalArgumentException("weight & price must be positive and less or equal than 100...");
        }
        this.weight = weight;
        this.price = price;
    }

    public int getIndex() {
        return index;
    }

    public double getWeight() {
        return weight;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public int compareTo(Thing o) {
        return Double.valueOf(this.getWeight()).compareTo(Double.valueOf(o.getWeight()));
    }
}

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created with IntelliJ IDEA.
 * User: ptoth
 * Date: 6/24/14
 * Time: 2:58 PM
 */
public class KnapsackInstance {

    private int maxWeight;

    private ArrayList<Thing> things;

    private ArrayList<String> optimalSolution;

    private int numberOfThings;

    private double[] prices;

    private double[] weights;

    private double[][] opt;

    private boolean[][] sol;

    public KnapsackInstance(int maxWeight, ArrayList<Thing> things) {
        this.maxWeight = maxWeight;
        this.things = things;
        this.initiateInstance();

    }

    public KnapsackInstance(int maxWeight) {
        this.maxWeight = maxWeight;
        this.things = new ArrayList<Thing>();
    }

    public void addThing(Thing thing) {

        this.things.add(thing);
    }

    public void initiateInstance() {

        this.numberOfThings = this.things.size();
        this.prices = new double[this.numberOfThings + 1];
        this.weights = new double[this.numberOfThings + 1];
        opt = new double[this.numberOfThings + 1][this.maxWeight + 1];
        sol = new boolean[this.numberOfThings + 1][this.maxWeight + 1];
        this.optimalSolution = new ArrayList<String>();

        Collections.sort(this.things);
        for(int i = 1; i <= this.numberOfThings; i++) {
            prices[i] = things.get(i-1).getPrice();
            weights[i] = things.get(i-1).getWeight();
        }
    }

    /**
     * the main method using dynamic programming
     * opt[n][w] contains the optimal price using n items with maxWeight w
     */
    private void solveProblem() {

        for (int n = 1; n <= this.numberOfThings; n++) {
            for (int w = 1; w <= this.maxWeight; w++) {

                // don't take item n
                double option1 = opt[n-1][w];

                // take item n
                double option2 = -0.001;
                if (weights[n] <= w) {
                    option2 = prices[n] + opt[n-1][w-(int)Math.round(weights[n])];
                }

                // select better of two options
                opt[n][w] = Math.max(option1, option2);
                sol[n][w] = (option2 > option1);
            }
        }
    }

    private boolean[] whichThingsToTake() {

        // determine which items to take
        boolean[] take = new boolean[this.numberOfThings+1];
        for (int n = this.numberOfThings, w = this.maxWeight; n > 0 && w >= 0; n--) {
            if (sol[n][w]) { take[n] = true;  w = w - (int)Math.round(weights[n]); }
            else           { take[n] = false;                    }
        }

        return take;
    }

    public ArrayList<String> getSolution() {

        solveProblem();
        boolean[] whichToTake = whichThingsToTake();
        for(int i = 1; i <= this.numberOfThings; i++) {
            if(whichToTake[i]) {
                optimalSolution.add(this.things.get(i-1).getIndex() + "");
            }
        }

        return this.optimalSolution;
    }
}

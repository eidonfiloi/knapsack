/**
 * Created with IntelliJ IDEA.
 * User: ptoth
 * Date: 6/24/14
 * Time: 2:57 PM
 */
public class Package {

    private int index;

    private int weight;

    private double price;

    public Package(int index, int weight, double price) {
        this.index = index;
        this.weight = weight;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Package{" +
                "index=" + index +
                ", weight=" + weight +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Package)) return false;

        Package aPackage = (Package) o;

        if (index != aPackage.index) return false;
        if (Double.compare(aPackage.price, price) != 0) return false;
        if (weight != aPackage.weight) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = index;
        result = 31 * result + weight;
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}

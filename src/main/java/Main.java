import au.com.bytecode.opencsv.CSVWriter;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: ptoth
 * Date: 6/24/14
 * Time: 3:02 PM
 */
public class Main {

    private static Logger logger = Logger.getLogger(Main.class);

    private static String INPUT_FILE = "src/main/resources/packages.txt";

    private static String OUTPUT_FILE = "";

    private static BufferedReader reader;

    private static CSVWriter writer;

    public static void main(String[] args) throws IOException {

        /**
         * determine input and/or output file paths
         */
        if(args.length == 2) {
            INPUT_FILE = args[0];
            OUTPUT_FILE = args[1];
        } else if(args.length == 1) {
            INPUT_FILE = args[0];
        } else {
            logger.info("Run in test mode...give input and/or output file paths as arguments...");
        }

        try {

            reader = new BufferedReader(new FileReader(INPUT_FILE));
            if(!OUTPUT_FILE.equals("")) {
                writer = new CSVWriter(new FileWriter(OUTPUT_FILE),',',CSVWriter.NO_QUOTE_CHARACTER);
            }

            /**
             * pattern for parsing items in parenthesis
             */
            Pattern p = Pattern.compile("\\((.*?)\\)",Pattern.DOTALL);

            String nextLine;
            packagesWhile:
            while((nextLine = reader.readLine()) != null) {
                /**
                 * read input file line by line
                 * split by :
                 * determine and check value of maxWeight
                 */
                String[] lineArr = nextLine.split(":");
                Integer currentMaxWeight = Integer.parseInt(lineArr[0]);
                if(currentMaxWeight <= 0 || currentMaxWeight > 100) {
                    logger.error("The current max weight value is invalid. It must be positive and less or equal to 100...");
                    continue packagesWhile;
                }
                Matcher matcher = p.matcher(lineArr[1]);
                KnapsackInstance currentInstance = new KnapsackInstance(currentMaxWeight);

                /**
                 * for each line we create a KnapsackInstance
                 * for each items found in a line create a Thing object
                 * after adding all Thing object, solve the knapsack problem
                 * get solution as a list of indices of things
                 */
                while(matcher.find()) {

                    String[] thingStrArr = matcher.group(1).split(",");
                    Thing thing = new Thing(Integer.parseInt(thingStrArr[0]), Double.parseDouble(thingStrArr[1]), Double.parseDouble(thingStrArr[2]));
                    currentInstance.addThing(thing);
                    logger.debug(matcher.group(1));
                    logger.debug(currentMaxWeight);
                }

                currentInstance.initiateInstance();
                ArrayList<String> currentSolution = currentInstance.getSolution();
                if(writer != null) {
                   writer.writeNext(currentSolution.isEmpty() ? new String[]{"-"} : currentSolution.toArray(new String[0]));
                } else {
                    logger.info(currentSolution);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());

        } finally {

            if(reader != null) {
                reader.close();
            }
            if(writer != null) {
                writer.close();
            }

        }

    }
}
